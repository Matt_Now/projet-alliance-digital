# Projet Alliance Digital



## Pour Commencer

Ce projet est réalisé avec Symfony, il est donc important d'avoir au préalable installé Symfony version 5.
Ensuite inséré les lignes suivantes dans votre invite de commande de votre IDE : 

-npm install
-npm run build
-composer install

## Technologies utilisées

Ce projet utilise EasyAdmin pour le controle des CRUD pour les administrateurs. 
Il Utilise aussi Full Calendar pour la mise en dispotion du Calendrier interractif disponible depuis la page d'accueil. 

Il y'a donc en language utilisé : HTML, CSS, Javascript, PHP, Symfony. 

## Parcourir le projet 

L'utilisateur attéri automatiquement sur la page de connexion, ça l'empêche donc d'être directement sur la page d'accueil, et de pouvoir modifié le calendrier, intéragir comme il le souhaite. 

L'admin Crée les utilisateur et les taches, personnes d'autres.

Il n'y a donc pas besoin de page d'inscription.

## Pour les futures versions 

J'aimerais sécurisé plus les accès aux pages si l'utilisateur n'est pas connecté/Admin. 
J'aimerais intégré un système de mot de passe oublié. 
Intégré les clients dans les taches, ce qui n'as pas été fait par manque de temps. 
Améliorer le design et le côté UI/UX de l'application. 
Créer un système de récurrence pour les tâches. 

J'aimerais que l'application n'est pas besoin d'internet dans le futur pour faciliter l'usage aux techniciens. 

## En espérant avoir retenu votre attention, durant la présentation du projet.