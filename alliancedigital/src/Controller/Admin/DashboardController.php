<?php

namespace App\Controller\Admin;

use App\Entity\Calendar;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Alliancedigital - Administration')
            ->renderContentMaximized();
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToRoute('Retour à la page accueil', 'fa fa-home', 'app_home');
        yield MenuItem::linkToCrud('Utilisateurs', 'fa fa-address-card', User::class);
        yield MenuItem::linkToCrud('Calendrier', 'fa fa-calendar-days', Calendar::class);
    }
}
