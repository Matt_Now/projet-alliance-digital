<?php

namespace App\Controller\Admin;

use App\Entity\Calendar;
use Container8CQVLs6\getFosCkEditor_Form_TypeService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use FOS\CKEditorBundle\Form\Type\CKEditorType;


class CalendarCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Calendar::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInPlural('Tâches')
            ->setEntityLabelInSingular('Tâches')
            ->setPageTitle('index', 'Tableau de bord Amdministration des tâches')
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');
    }

    public function configureFields(string $pageName): iterable
    {
        yield Field::new('title')->setLabel('Titre');
        yield Field::new('Fait')->setLabel('Effecttué?');
        yield Field::new('description')
        ->hideOnIndex()
        ->setLabel('Description');
        yield Field::new('start')->setLabel('Début');
        yield Field::new('end')->setLabel('Fin');
        yield Field::new('all_day')->setLabel('Jour Entier');
        yield Field::new('background_color')->setLabel('Couleur de fond');
        yield Field::new('border_color')->setLabel('Couleur de la bordure');
        yield Field::new('text_color')->setLabel('Couleur du texte');
    }
}
